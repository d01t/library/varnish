ARG VERSION=latest

FROM ubuntu:${VERSION}

COPY install.sh /root/install.sh

RUN /bin/bash /root/install.sh

VOLUME ["/var/lib/varnish"]

EXPOSE 80

CMD ["/start.sh"]
