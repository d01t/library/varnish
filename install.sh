#!/bin/bash
# Stop on error
set -e
# Install mandatory programs and utilities
export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get --yes install curl varnish
curl -s -o /etc/varnish/default.vcl https://raw.githubusercontent.com/mattiasgeniar/varnish-6.0-configuration-templates/master/default.vcl
apt-get remove --yes curl
apt-get autoremove --yes
apt-get clean
sed -i -E 's/host = "[^"]+"/host = "web"/g' /etc/varnish/default.vcl
sed -i -e 's#HEAD / HTTP/1.1#HEAD /favicon.ico HTTP/1.1#g' /etc/varnish/default.vcl
sed -i -e "s/if (req.method == \"PURGE\") {/if (req.method == \"BAN\") {\n    # Same ACL check as above:\n    if (\!(client.ip ~ purge) \&\& \!(req.http.Authorization ~ \"DefaultSecretKey\")) {\n      return (synth(405, \"This IP is not allowed to send BAN requests.\"));\n    }\n    if (req.http.X-Cache-Tags) {\n      ban(\"obj.http.X-Host ~ \" + req.http.X-Host\n        + \" \&\& obj.http.X-Url ~ \" + req.http.X-Url\n        + \" \&\& obj.http.content-type ~ \" + req.http.X-Content-Type\n        + \" \&\& obj.http.X-Cache-Tags ~ \" + req.http.X-Cache-Tags\n      );\n    } elseif (req.http.X-Host \&\& req.http.X-Url) {\n      ban(\"obj.http.X-Host ~ \" + req.http.X-Host\n        + \" \&\& obj.http.X-Url ~ \" + req.http.X-Url\n        + \" \&\& obj.http.content-type ~ \" + req.http.X-Content-Type\n      );\n    }\n    else {\n      ban(\"req.http.host == \" + req.http.host);\n    }\n    # Throw a synthetic page so the\n    # request won't go to the backend.\n    return (synth(200, \"Ban added\" ));\n  }\n  if (req.method == \"PURGE\") {/g" /etc/varnish/default.vcl
sed -i -e "s/set req.url = std.querysort(req.url);/if ( ( ! req.http.host ~ \"phpmyadmin\" ) \&\& ( ! req.http.host ~ \"phppgadmin\" ) \&\& ( ! req.url ~ \"wp-admin\" ) ) {\n    set req.url = std.querysort(req.url);\n  }/g" /etc/varnish/default.vcl
sed -i -e "s#hash_data(req.url);#hash_data(req.url);\n  if (req.http.X-Forwarded-Proto) {\n    hash_data(req.http.X-Forwarded-Proto);\n  }#g" /etc/varnish/default.vcl
sed -i -e 's/set bereq.http.upgrade = req.http.upgrade;/set bereq.http.upgrade = req.http.upgrade;'"\n"'    set bereq.http.connection = req.http.connection;/g' /etc/varnish/default.vcl
sed -i -e 's/req.http.Upgrade/req.http.upgrade/g' /etc/varnish/default.vcl
sed -i -e "s/sub vcl_backend_response {/sub vcl_backend_response {\n  set beresp.http.X-Url = bereq.url;\n  set beresp.http.X-Host = bereq.http.host;/g" /etc/varnish/default.vcl
touch /etc/varnish/secret
cat <<EOF > /start.sh
#!/bin/bash
if [ -n "\${SERVICE_WEB}" ]
then
	sed -i -E 's/host = "[^"]+"/host = "'"\${SERVICE_WEB}"'"/g' /etc/varnish/default.vcl
fi
if [ -n "\${VARNISH_SECRET}" ]
then
	sed -i -E 's/"DefaultSecretKey"/"'"\${VARNISH_SECRET}"'"/g' /etc/varnish/default.vcl
	echo -n "\${VARNISH_SECRET}" > /etc/varnish/secret
fi
/usr/sbin/varnishd -j unix,user=varnish -F -f /etc/varnish/default.vcl -p feature=+http2 -a 0.0.0.0:80 -s malloc,1g
EOF
chmod +x /start.sh
